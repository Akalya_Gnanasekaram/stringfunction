package edu.bcas.str;

public class BooleanEqualsObj {
	public static void main(String[] args) {

		String S1 = new String("A class must have data funtion");
		String S2 = S1;
		String S3 = new String("A class must have data funtion");
		boolean retVal;

		retVal = S1.equals(S2);
		System.out.println("Returned Value = " + retVal);

		retVal = S1.equals(S3);
		System.out.println("Returned Value = " + retVal);
	}

}
