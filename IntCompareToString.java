package edu.bcas.str;

public class IntCompareToString {

	public static void main(String[] args) {
		String a1 = "A class must have data funtion";
		String a2 = "A class must have data funtion";
		String a3 = "Welcome";

		int a = a1.compareTo(a2);
		System.out.println(a);

		a = a2.compareTo(a3);
		System.out.println(a);

		a = a3.compareTo(a1);
		System.out.println(a);
	}

}