package edu.bcas.str;

public class StringDemo {
	public static void main(String[] args) {
		// method1
		String msg = "Welcome";
		String campus = " to BCAS";
		System.out.println(msg);

		// find char
		char value = msg.charAt(3);
		// 1
		System.out.println(value);
		// 2
		System.out.println(msg.charAt(0));
		// 3
		System.out.println("" + msg.charAt(3) + msg.charAt(5) + msg.charAt(6));

		// concat1
		System.out.println((msg.concat(campus)).concat(" Campus"));
		// concat2
		String s1 = msg.concat(campus);
		String s2 = s1.concat(" Campus");
		System.out.println(s2);

		// to check the length of the string
		System.out.println("length of string s2 is :" + s2.length());

	}

}
