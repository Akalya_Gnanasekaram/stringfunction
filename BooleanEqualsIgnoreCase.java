package edu.bcas.str;

public class BooleanEqualsIgnoreCase {

	public static void main(String[] args) {
		
				String a1 = new String("A class must have data funtion");
				String a2 = a1;
				String a3 = new String("A class must have data funtion");
				String a4 = new String("A class must have data funtion");
				boolean retVal;

				retVal = a1.equals(a2);
				System.out.println("Returned Value = " + retVal);

				retVal = a1.equals(a3);
				System.out.println("Returned Value = " + retVal);

				retVal = a1.equalsIgnoreCase(a4);
				System.out.println("Returned Value = " + retVal);
			}

		}


