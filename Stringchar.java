package edu.bcas.str;

public class Stringchar {

	public static void main(String[] args) {
		// charcharatintindex

		String S = "Computer";
		
		//method 1
		char S1 = S.charAt(3);
		System.out.println(S1);
		
		//method 2
		System.out.println("character at the index 3 is : " + S.charAt(3));
	}

}
