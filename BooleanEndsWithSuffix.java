package edu.bcas.str;

public class BooleanEndsWithSuffix {

	public static void main(String[] args) {
		String x = new String("A class must have data funtion");
		boolean returnVal;

		returnVal = x.endsWith("data funtion");
		System.out.println("Returned Value = " + returnVal);

		returnVal = x.endsWith("ta");
		System.out.println("Returned Value = " + returnVal);

	}

}
