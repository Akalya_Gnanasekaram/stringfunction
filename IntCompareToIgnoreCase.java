package edu.bcas.str;

public class IntCompareToIgnoreCase {

	public static void main(String[] args) {
		String s1 = "A class must have data funtion";
		String s2 = "A class must have data funtion";
		String s3 = "Welcome";

		int S = s1.compareToIgnoreCase(s2);
		System.out.println(S);

		S = s2.compareToIgnoreCase(s3);
		System.out.println(S);

		S = s3.compareToIgnoreCase(s1);
		System.out.println(S);
	}

}
